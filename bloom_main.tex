% !TEX root = bloomfeatures.tex

\section{Learning with Memory Constraints}\label{sec:intro}
The use of randomness
in order to save some
resource such as memory (or computation time) at the potential expense of
accuracy is an established strategy throughout computer science. A primary application
of these low-memory footprint methods is in \emph{large-scale} settings in which
one must process an extremely large amount of possibly high-dimensional data.
In this situation the original dataset may not fit into memory and so one turns
to methods that reduce the memory footprint of the dataset but also allow the user
to run some type of analysis on the data.

In
machine learning, these memory savings are achieved by methods that use random projections \cite{johnson1984extensions} 
to approximate some function $f:\R^d\to \R$ using a small number of parameters,
but state of the art  methods suffer from particular drawbacks.
Random Fourier
features \cite{Rahimi2007random} has poor asymptotic time complexity; Vowpal Wabbit (VW)
\cite{weinberger2009featurehashing} can only approximate linear functions; and $b$-bit Minwise hashing \cite{li2010b} 
can only operate on binary inputs.

We present an algorithm that achieves a low memory footprint while avoiding these
drawbacks.
Similar to the previous methods, 
for any $m$ we produce a mapping $\phi:\R^d\to \R^m$ such that as $m$ increases
linear functions on $\R^m$ approximate nonlinear functions in $\R^d$ with error
inversely proportional to $m$.
However our memory footprint increases linearly with $m$, 
and so $m$ represents a trade-off between memory
and accuracy in function fitting.

We call the vectors in $\R^m$ produced by our mapping Bloom features (Definition \ref{dfn:bloomfeature}), as our method was
inspired by the Bloom filter data structure \cite{bloom1970space}. 
To compute Bloom features, we choose $k$
hash functions to assign each component of a vector $x$ in $\R^d$ to $k$ components 
of $\phi(x)$ in $\R^m$. We compute the $i$th component of $\phi(x)$ by taking the MAX of all the components
of $x$ that are mapped to this $i$th component (Figure \ref{fig:bloomfeature}). This is analogous to multiplication by a sparse
$m\times d$ matrix (specified by the $k$ hash functions) with addition replaced by MAX.
Using hash functions and sparsity
allows us to have low memory footprint and small time complexity while
the substitution of MAX for addition allows us to compute non-linear
functions on real-valued inputs.
\vspace{-4pt}

\begin{figure}[H]
\begin{center}
\begin{tikzpicture}[>=stealth] 
\matrix (M) [matrix of nodes,% 
   column sep=0pt,% 
   row sep=4mm,% 
   nodes={draw,%
     minimum width=.5cm, outer sep=0pt,% 
     minimum height=.5cm, anchor=center}, 
   column 1/.style={   column sep=0pt,% 
   row sep=3mm,% 
   nodes={draw=none,%
     minimum width=.5cm, outer sep=0pt,% 
     minimum height=.5cm, anchor=center}},
   column 2/.style={   column sep=0pt,% 
   row sep=3mm,% 
   nodes={draw=none,%
     minimum width=.5cm, outer sep=0pt,% 
     minimum height=.5cm, anchor=center}},
   column 10/.style={   column sep=0pt,% 
   row sep=3mm,% 
   nodes={draw=none,%
     minimum width=.5cm, outer sep=0pt,% 
     minimum height=.5cm, anchor=center}}]
{ 
  $x$: &$\cdots$ &0&1&0&3&7&0&0&$\cdots$\\%
 $\phi(x)$:&&1&0&3&7&3&0&7&\\
}; 
\draw[->] (M-1-4.south)--(M-2-3.north);
\draw[->] (M-1-4.south)--(M-2-5.north);
\draw[->] (M-1-6.south)--(M-2-5.north);
\draw[->] (M-1-6.south)--(M-2-7.north);
\draw[->] (M-1-7.south)--(M-2-6.north);
\draw[->] (M-1-7.south)--(M-2-9.north);
\end{tikzpicture} 
\caption{Each coordinate of $x$ is mapped to $k$ random coordinates in $\phi(x)$ by
  hash functions. The final value of each coordinate of $\phi(x)$ is
  given by taking the maximum of the values hashed to it. In this case
  $k=2$ and $m=7$.}
\label{fig:bloomfeature}
\end{center}
\end{figure}

In Section \ref{sec:alg}, we introduce Bloom features and describe their properties.
%, including a short
%introduction to the Bloom filter.
In Section \ref{sec:emp}, we empirically validate Bloom features
on the freely available 
\texttt{MNIST} \cite{lecun1998gradient}, and
\texttt{webspam} \cite{webb2006introducing} datasets. We also provide results
on simulated data. We state formal results and proofs in an appendix.
\vspace{-1pt}
\section{Bloom Features}\label{sec:alg}
\vspace{-1pt}
Approximating functions with Bloom features offers a good tradeoff between space, time and accuracy. Given input/output pairs
$(x_i,y_i)\in \R^d\times \R$, this problem may be formulated as follows: Find $f_w:\R^d\to \R$ of the form 
$f_w(x)= \langle \phi(x), w\rangle$ where $\langle a, b\rangle$ indicates inner-product,
such that $f_w(x_i)\approx y_i$, where $\phi:\R^d\to \R^m$ is a fixed map and $w\in \R^m$ is
chosen to achieve the best approximation.
The degree of approximation is
measured by some loss function $L$. For example, $L( f_w,x,y)=(f_w(x)-y)^2$ is a
common choice. Linear regression can be formulated in this manner
with $\phi(x)=x$ and quadratic-kernel support vector regression corresponds
to $\phi(x)$ equal to a vector of all $2^\text{nd}$ order interactions in $x$.

Formulating function-approximation as a linear-map on a set of $m$-dimensional features $\phi(x)$, provided by a fixed mapping $\phi$, introduces a tradeoff between resources and accuracy. As $m$ increases,
$\phi$ maps into higher dimensional spaces, resulting in
more accurate function-fitting through inner products with $\phi(x)$, but
requiring more time and space to be computed.
A desirable $\phi$ will obtain a good tradeoff between space, time and accuracy.

%we $\phi$ is 
%Often we need to be able
%to compute $\phi(x)$ very quickly. In the online scenario this could be due to
%some need to provide approximations in real-time.
%In the batch case computing $\phi$ quickly results in faster training and evaluation
%times. We might also want to minimize inductive bias: that is, there
%should actually be a $w$ such that 
%$\langle\phi(x),w\rangle\approx y$. We present Bloom features as a
%candidate for $\phi$ that fulfills both constraints.
The Bloom feature map achieves a good tradeoff by using hash functions to specify a sparse matrix and replacing the addition in matrix multiplication by MAX. This map $\phi$ operates on $d$-dimensional non-negative inputs---that is, elements of $\R_{\ge 0}^d$. It can approximate any linear combination of binary OR functions or
real-valued MAX functions with error that decreases inversely proportional to $m$ 
(Theorems \ref{thm:binary}, \ref{thm:kernel}) and can be computed in $O(m)$ space and $O(m)$ time (Proposition \ref{thm:speed}). While its space scaling is the same as three state-of-the-art methods, its time scaling is $n$ to $n/m$ times better (Table \ref{tbl:speed}).
\begin{table}[H]
\caption{Comparison between Methods}\label{tbl:speed}
\vspace{-12pt}
\begin{center}
\begin{tabular}{|c|c|c|c|}
    \hline
    Feature&Time&Non-Binary&Non-linear\\
    \hline
    Bloom &$O(m)$&Yes&Yes\\
    \hline
    Fourier&$O(nm)$&Yes&Yes\\
    \hline
    $b$-bit Minwise&$O(\frac{nm}{2^b})$&No&Yes\\
    \hline
    VW&$O(n)$&Yes&No\\
    \hline
\end{tabular}
\end{center}
\end{table}
\section{Empirical Results}\label{sec:emp}
To verify that Bloom features' resource scaling factors are reasonable and confirm that its theoretical performance generalizes to the real-world, we tested it on simulated data (binary and real-valued) and real data (\texttt{MNIST} and \texttt{webspam}), respectively. 

\subsection{Simulated Data}
We first tested Bloom features on a simulated classification task with binary
vectors. The binary vectors were drawn from $\{0,1\}^{100}$ with $10$ randomly
selected non-zero entries. The two classes were defined by an OR function
computed on either $3$, $5$ or $10$ randomly chosen bits. We chose a subset of
the data that had an equal fraction belonging to each class.
We used Bloom features with $m = 100,\; 1000,\; 10000$ and $k=\lfloor \frac{m}{10}\log(2)\rfloor$, the optimal value for a Bloom filter.
We found that Bloom features achieve low MSE using $m$ values significantly smaller
than ${100 \choose F}$, the number of
possible OR functions of fan-in $F$ (Figure \ref{fig:mseplot}).

\begin{figure}[H]
\begin{center}
\includegraphics[height=1.8in]{mseplot.eps}
\caption{Mean squared error vs $m$ for a boolean OR of fan-in $3$ and
  $10$, with $d=100$. Horizontal lines indicate best linear regression error for
  comparison. Error bars represent one standard deviation.}
\label{fig:mseplot}
\end{center}
\end{figure}

% Please explain in more detail. What's a Dirichlet distribution. How do you create a data set from two of them?
Next, we tested Bloom features on a simulated real-valued classification task
consisting of two classes defined by two distinct, $100$-dimensional Dirichlet
distributions with parameters
$\alpha$ and $\frac{3\alpha+\beta}{4}$ respectively. $\alpha$ and $\beta$ were drawn
uniformly at random from $(0,1)^{100}$. This particular choice results in a
Bayes risk  of $0.3\%$ (error of optimal classifier).  We compared Bloom features to random
Fourier features with $m$ ranging from 50 to 10000 for both. $k$ was set to $\lfloor \frac{m}{10}\log(2)\rfloor$ for Bloom features; $b$-bit Minwise hashing was not included because this method only applies to binary inputs. We found that Fourier features were more accurate for small $m$ whereas Bloom features were more accurate for large $m$ (Figure \ref{fig:dirichletsim}).

\begin{figure}[H]
    \begin{center}
        \includegraphics[height=1.8in]{bloom_simulated_errorbars.eps}
        \caption{Bloom features vs. random Fourier Features on simulated dataset. Error bars represent one standard deviation.}
    \label{fig:dirichletsim}
    \end{center}
\end{figure}

We then proceeded to test Bloom features on more realistic data by
classifying the \texttt{MNIST} \cite{lecun1998gradient} and
\texttt{webspam} \cite{webb2006introducing} datasets.
%and \texttt{covtype}
%\cite{Lichman2013} datasets.
% Both of these datasets include sparse feature
% vectors. As is well-known, linear classifiers can achieve only limited
% performance on \texttt{MNIST} (about $7\%$ error), while \texttt{webspam} is
% nearly linearly separable. 
For all tasks, we applied one-vs-all linear
classification on a Bloom feature representation of the data.

\subsection{MNIST}
The \texttt{MNIST} dataset contains $70,000$ $28\times 28$ gray-scale images
of handwritten digits 0 through 9, which are split into a training and
test set of size $60,000$ and $10,000$ respectively. The task is to determine
which digit a given image represents. We consider the permutation-invariant form of the task, 
in which one cannot take advantage of any
previously known structure in the data (such as the fact that the inputs are 2D images).
We chose $k$ via cross-validation on $m=400$, which gave
$k=4$. We therefore used $k=m/100$ (rounded to
the nearest integer) for all other values of $m$. 

We compared Bloom features' performance on \texttt{MNIST} with that of random Fourier Features and $b$-bit Minwise
hashing, which have the same memory footprint. We did not include VW because it can only classify linearly separable data; it is well-known that $\texttt{MNIST}$ is not linearly separable. In the case of Minwise hashing, 
we converted the images to binary vectors
by thresholding values to $1$, an easy and natural process for \texttt{MNIST}. For other datasets (e.g. our simulation in
Figure \ref{fig:dirichletsim}), this may not be true. We found that the performance gap between Bloom and Fourier features narrowed as $m$ increased (Table \ref{tbl:mnist}), as expected from our results with simulated data (Figure \ref{fig:dirichletsim}). And both out-performed Minwise hashing, demonstrating the advantage of using real-valued vectors. 

We also compared Bloom features' performance on \texttt{MNIST} with several
state-of-the-art algorithms that have a much larger memory footprint as measured
by parameter count (taken from
Table 1 of \cite{goodfellow2013maxout}). One of these algorithms (Maxout MLP,
described in \cite{goodfellow2013maxout}) is similar in spirit to Bloom
features. In this method, one trains a multi-layer perceptron whose activation
function takes the maximum of its inputs. We found that Bloom features use
$95\%$ fewer parameters than any of these algorithms while achieving an
error-rate within a factor of two of the lowest result ($1.6\%$ versus
$0.79\%$).

\begin{table}[H]
    \caption{\texttt{MNIST} Performance (average error)}\label{tbl:mnist}
\begin{center}
\begin{tabular}{c|c|c|c}
    $m$&Bloom & Fourier &$b$-bit Minwise\\
    \hline
%    $m=200$&8.6\%&2000\\
%    $m=300$&6.4\%&3000\\
    $400$&6.3\%&5.3\%& 11.1\%\\
    $1000$&3.8\%&3.1\%&7.4\%\\
%    $m=5000$&2.8\%&50000\\
%    $m=10000$&2.2\%&100000\\
    $10000$&1.8\%&1.6\%&2.8\%\\%50000
\end{tabular}
\end{center}
\end{table}

\begin{table}[H]
\caption{\texttt{MNIST} Performance (Other Methods)}\label{tbl:compmnist}
\vspace{-15pt}
\begin{center}
\begin{tabular}{c|c|c}
Method&Error&Parameter Count\\
\hline
ReLU MLP \cite{srivastava2013improving} & 1.05\% & $1794000$\\
Maxout MLP \cite {goodfellow2013maxout} & 0.94\% &$2392800$\\
Manifold Tangent Classifier \cite{rifai2011manifold} & 0.81\% &$5588000$\\
DBM (with dropout) \cite{hinton2012improving} & 0.79\% & $2392800$\\
Bloom Features &1.6\%&$100000$
\end{tabular}
\end{center}
\end{table}

\subsection{webspam}
The \texttt{webspam} \cite{webb2006introducing} dataset consists of 350000 sparse vectors of trigram
counts representing either spam or non-spam documents. The data have a
dimensionality of 16609143, of which only 680715 components are ever nonzero.
\texttt{webspam} is nearly linearly separable, with linear classification accuracies
in excess of $99\%$. Thus, we consider classifying \texttt{webspam} as a
demonstration that Bloom features are still able to capture linear
relationships. To classify \texttt{webspam} we
selected $80\%$ of the data to be a training set and $20\%$ to be a
testing set.

We compared Bloom features' performance on \texttt{webspam} with that of random Fourier Features, $b$-bit Minwise
hashing, and VW for $m=100, 1000, 10000$. All three choices use much less memory than is needed to represent the
original features (up to 10000 vs 680715). $\texttt{webspam}$'s sparse binary features and linearly separability are exactly the conditions required by $b$-bit Minwise hashing and VW, respectively, hence they performed the best (Table \ref{tbl:webspam}). Nevertheless, across the range of $m$ values tested, Bloom Features' error was no worse than 1.625 times that of these methods. 

%In regards to the $b$-bit Minwise hashing performance, note that we are comparing
%to the \emph{total feature dimensionality}, which is distinct from the total amount 
%of space a feature vector takes in memory. $b$-bit Minwise hashing's major gains come
%from the use of bit-manipulation to store binary feature vectors. However, in an online
%setting the major memory overhead is the learned weight vector, which is determined
%but the dimensionality of the features.
% As an
% interesting note, empirically we observed that creating an $m$-dimensional
% feature vector similar to $\phi_z(x)$ by subtracting half of the features
% in a $2m$-dimensional Bloom feature map from the other half tended to
% result in marginally better performance. However to maintain consistency
% with the \texttt{MNIST} results we report the unmodified Bloom feature map.

\begin{table}[H]
  \caption{\texttt{webspam} Performance (Average Error)}\label{tbl:webspam}
\begin{center}
\begin{tabular}{c|c|c|c|c}
    $m$&Bloom&Fourier&$b$-bit Minwise&VW\\ 
    \hline
    $100$&9.4\%&26.4\%&10.8\%&8.1\%\\
    $1000$&2.9\%&14.7\%&2.1\%&1.9\%\\
    $10000$&1.3\%&7.9\%&0.8\%&1.1\%
\end{tabular}
\end{center}
\end{table}

\section{Conclusions}
We introduced Bloom features and analyzed their performance. 
We showed that the Bloom feature representations are memory-efficient ($O(m)$) and can be
computed very quickly ($O(m)$). As their dimensionality $m$ increases, they
approximate boolean functions and non-linear real-valued functions 
with error decreasing inversely proportionally to $m$. We prove these results in the Appendix
and show that
Bloom features operate by
approximating a high-dimensional RKHS consisting of all
interactions among inputs.

% What are their theoretical advantages?
We demonstrated the practical viability of Bloom features
using simulated data and the \texttt{MNIST} and \texttt{webspam} datasets. 
On the simulated and \texttt{MNIST} datasets, Bloom features 
represented non-linear functions on real-valued inputs accurately and efficiently.
On the \texttt{webspam} dataset, Bloom features were 
competitive with more restrictive methods such as
$b$-bit Minwise hashing and VW when representing linear functions on binary inputs.
Thus Bloom features not only possess theoretical advantages in terms of either handling real-valued inputs,
computation time,
or non-linear approximation over
other methods, they also compare favorably 
on practical tasks in terms of test error for a given memory footprint.

%Often in constructing learning algorithms or statistical models one first
%creates a procedure that optimizes for the data and then creates an
%algorithm that implements this procedure or an approximation of it as
%efficiently as possible. This process results in accurate,
%but potentially slow algorithms. When one knows a-priori that computation
%is likely to be an issue, for example due to the size of the dataset or
%the necessity of providing rapid answers, it is a good idea to consider
%efficiency from the very beginning. 
%This style of investigation 
%to methods such as Bloom features, which combine computational
%efficiency with representational capacity.

