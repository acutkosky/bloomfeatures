% !TEX root = bloomfeatures.tex

\appendix
%section{Definitions and Proofs}

\begin{Definition}{\bf Bloom Features}\label{dfn:bloomfeature}
Suppose $h_1,\dots,h_{k}$ are functions mapping $\N$ to $\{0,\dots,m-1\}$ drawn at random
from a pair-wise independent family of hash functions.
For $x\in\R_{\ge 0}^d$, define
$\phi_h(x)\in \R_{\ge 0}^m$ by
\begin{align*}
\phi_h(x)_i = \max_{j,l:\ h_l(j)=i}x_j
\end{align*}
where the subscript $h$ in $\phi_h$ is intended to emphasize that
$\phi_h(x)$ depends on the choice of hash functions.
\end{Definition}
\begin{Proposition}{\bf Computation} \label{thm:speed}
    Suppose $x\in \R^d$ with $n$ non-zero components, and let $\phi_h(x)$ be a Bloom feature map
    using $k$ hash functions. Then $\phi_h(x)$ can be
computed in $O(kn)$ time when vectors are represented in sparse format
(only non-zero quantities stored), and in $O(d+kn+m)$ time when
vectors are represented in dense format.
\end{Proposition}
\begin{Proposition}{\bf Memory} \label{thm:mem}
Standard Bloom filter analysis 
\cite{mitzenmacher2005probability} suggests a value for $k$:
If our inputs $x$ have $n$ non-zero components, then we should
set $k=\log(2)\frac{m}{n}$. 
Using this value of $k$ and Proposition \ref{thm:speed}, we get a complexity of $O(kn)=O(m)$.
\end{Proposition}
\begin{Theorem}{\bf Binary Input Approximation}\label{thm:binary}
  Suppose $B(x)$ is a boolean function of fan-in $F$ and $\phi_h(x):\R^d\to \R^m$ is a Bloom
  feature map with $k$ hash functions (see Definition \ref{dfn:bloomfeature}). 
  Let $n$ be the number of set bits of some vector $x\in \{0,1\}^d$.
  Let $p=1-(1-\frac{1}{m})^k$ be the probability that a bit of $x$ is hashed to
  a given component of $\phi_h(x)$. 
  %Let
  %$Q_n=1-(1-p)^n$ be the expected
  %fraction of non-zero entries in
  %$\phi_h(x)$. 
  Then for sufficiently large $m$ there
  exists $w_h\in\R^m$ such that
  \begin{gather*}
    \E_{h}[\phi_h(x)\cdot w_h] = B(x)\\
    \E_{h}[(\phi_h(x)\cdot w_h-B(x))^2] =
    O\left(\tfrac{2^F}{p^F(1-1/m)^{kn}m}\right)
  \end{gather*}
\end{Theorem}
\begin{proof}
  Our proof has two steps. First we observe that we can write $B(x)$ as a
  linear combination of {\sc or}s involving no negations:
  $B(x)=\sum_i^s a_i(x_{i_1}\vee \cdots\vee x_{i_z})$ with $a_i\in\R$, $z\le F$ and
  $s\le 2^F$. 
 % That is,
 % $B(x)$ is a linear combination of $\texttt{OR}$
 % functions involving no negations:
 % $B(x)=\sum_{i}^{2^F} a_i \texttt{OR}_i(x)$.
  Then we show that
  $x_{i_1}\vee\cdots\vee x_{i_z}\approx \phi_h(x)\cdot w_{h,i}$ for some
  appropriate $w_{h,i}$ (see Lemma \ref{thm:patch}). Substituting
  this approximation in to the expression for $B(x)$ completes the proof with
  $w_h = \sum_{i} a_i w_{h,i}$.
  
  To prove the first step, consider all functions on $\{0,1\}^F$ as elements
  of $\R^{2^F}$ defined by their truth tables. Then to show the first step we
  need only show that the set of {\sc or} functions are linearly independent in
  this space. To see this, first set $y_j=1-x_j$. Then by De Morgan's rules we
  have $x_{i_1}\vee \cdots\vee x_{i_z} =
  1-(1-x_{i_1})\cdots(1-x_{i_z})=1-y_{i_1}\cdots y_{i_z}$.
  Therefore the {\sc or}
  functions are linearly independent if and only if the products
  $y_{i_1}\cdots y_{i_z}$ are. But these are all the distinct monomials in $y_j$ and
  so must be linearly independent.
  %For the first step, consider \texttt{OR} expressions involving no negations.
  %That is, expressions
  %of the form $x_{i_1}\vee x_{i_2}\vee x_{i_z}$. The set of all such
  %$\texttt{OR}$ functions is linearly independent. Therefore any function of $x$
  %(and in particular $B(x)$) can be written as a linear combination
  %$\sum_{i=1}^{2^F} a_i\texttt{OR}_i(x)$ where $\texttt{OR}_i$ ranges over all
  %$2^F$ \texttt{OR} functions.
  %The second step, and the rest of the Theorem, follows immediately from Lemma
  %\ref{thm:patch}.
\end{proof}

\begin{Lemma}\label{thm:patch}
    Suppose $C^1(x)$ and $C^2(x)$ are binary OR functions of fan-in $F^1$ and $F^2$. Further, define $F^\cup$
    as the number of inputs shared by $C^1$ and $C^2$.
    Let $p = 1-(1-\frac{1}{m})^k$ and let $\overline{F} = (F^1+F^2)-F^\cup$.
    Suppose $\phi_h$ is a Bloom feature map with $k$ hash functions,
    $x\in\{0,1\}^d$ has $n$ non-zero bits, $p=1-(1-\frac{1}{m})^k$ and
    $Q_n=1-(1-p)^n$ (see Theorem \ref{thm:binary}).
    Then for sufficiently large $m$ there exists $w^1_h$ and $w^2_h$ such that:
    \begin{gather*}
\E_{h}[\phi_h(x)\cdot w^i_h] = C^i(x)\\
\var(\phi_h(x)\cdot w^i_h) \le \tfrac{4}{m}p^{-F^i}(1-Q_n)^{-1}\\
\cov(\phi_h(x)\cdot w^1_h,\phi_h(x)\cdot w^2_h)  \le \tfrac{2}{m}
    p^{-\overline{F}}(1-Q_n)^{-1}
\end{gather*}
%where $Q_n=1-(1-p)^n$.
\end{Lemma}
\begin{proof}
  First we introduce an easier-to-analyze
  but harder-to-compute version of Bloom features with feature map $\phi_z$ (see
  Definition \ref{dfn:independentfeature}). Then we 
  establish a result analogous to Lemma \ref{thm:patch} for
  $\phi_z$ with bounds improved by a factor of $4$ (see Lemma
  \ref{thm:biasvariance}).  Finally, we now show that the
  analysis for $\phi_z$ in Lemma \ref{thm:biasvariance} applies to $\phi_h$ with bounds relaxed by a factor of
  $4$.

We use $\phi_h$ to construct a ``hashed'' version of $\phi_z$ of dimensionality $m/2$ by subtracting the first $m/2$
components of $\phi_h$ from the second $m/2$ components. If
each component of $\phi_h$ was independent of the others, then Lemma
\ref{thm:biasvariance} applies and since we used $m/2$ instead of $m$ we
get a bound a factor of 2 worse than
in Lemma \ref{thm:biasvariance} (a factor of 2 better than we want to show).
It remains to bound the error introduced by the lack of independence by
a factor of 2.

Inspection of the proofs of Lemma \ref{thm:biasvariance} shows that the only use of the independence
assumption is to distribute expectations over products in $\E_h[T^\pm_iT^\pm_j]$ or $\E_h[T^+_iT^-_i]$. We will show that we can
distribute for hashed Bloom features as well while accruing a small error that goes down
quickly as $m$ increases. Let $e$ be the number of empty slots 
in $\phi_h(x)$ and $q = \frac{e}{m}$. Then $Q_n = 1-\E[q]$. Mitzenmacher and Upfal show \cite{mitzenmacher2005probability} 
that $P(|q-\E[q]|>\frac{\lambda}{m})<2\exp(-2\lambda^2/m)$. Thus
\begin{align*}
    \E[q^2] &\ge(1-2\exp(-2\lambda^2/m))(\E[q]-\lambda/m)^2\\
    \E[q^2]&\le(1-2\exp(-2\lambda^2/m))\times\\
           &\quad\quad(\E[q]+\lambda/m)^2+m^2(2\exp(-2\lambda^2/m))
\end{align*}
So that for any $\delta$, for sufficiently large $m$, we must have $\frac{1}{1-\delta}\E[q]^2\le \E[q^2]\le (1+\delta)\E[q]^2$. Thus
\begin{align*}
\E[T^+_iT^+_j] &= \sum_t p(q=t)(1-t)(1-t\tfrac{m}{m-1})\\
&=1-\tfrac{2m-1}{m-1}\E[q]+\tfrac{m}{m-1}\E[q^2]
\end{align*}
Thus for any $\delta'$, for sufficiently large $m$, $\E[T^+_iT^+_j]$
is within a factor of $1+\delta'$ of $(1-\E[q])^2=\E[T^+_i]\E[T^+_j]$. 
%Now $\E[T^+_i] =
%1-\E[q]$ so therefore $\E[T^+_iT^+_j]$ is within a factor of $1+\delta'$
%of $\E[T^+_i]\E[T^+_j]$.
Choosing $\delta'$ appropriately, for sufficiently large $m$
the error from the independence assumption becomes arbitrarily small.
\end{proof}

\begin{Definition}{\bf Independent Features}\label{dfn:independentfeature}
Let $p\in(0,1)$. Suppose $z^+_{i,j},z^-_{i,j}$ are independent Bernoulli
random variables with mean $p$ for $i\in\{0,\dots,m-1\}$, $j\in
\{0,\dots,d-1\}$. Then define
\begin{align*}
T^+(x)_i &= \max_{j:z^+_{i,j}=1}x_j\\
T^-(x)_i &= \max_{j:z^-_{i,j}=1}x_j\\
\phi_z(x)_i &= T^+(x)_i-T^-(x)_i
\end{align*}
where here the subscript $z$ indicates that $\phi_z$ depends on the values
of the variables $z^+_{i,j}$ and $z^-_{i,j}$.
\end{Definition}

\begin{Lemma}\label{thm:biasvariance}
  Let $C^1$ and $C^2$ be boolean \texttt{OR}s of fan-in $F^1$ and $F2$ respectively.
  Let $F^\cup$ be the number of inputs shared by $C^1$ and $C^2$. Let $\phi_z$
  be an independent features map with parameter $p$ (see Definition
  \ref{dfn:independentfeature}). Then there exists
  $w_z^1,w_z^2\in
  \R^m$ such that for $x\in\{0,1\}^d$ with
  $n$ non-zero entries:
\begin{equation}\label{eqn:unbiased}
\E_{z}[\phi_z(x)\cdot w_z^i] = C^i(x)
\end{equation}
Where $Q_n=1-(1-p)^n$.
%is the probability that $\phi_z(x)_{i}=1$ given that $x$ has $n$
%non-zero entries:
%$Q_n=1-(1-p)^n$.
We also have
\begin{gather*}
\var(\phi_z(x)\cdot w_z^i) \le \tfrac{1}{m}p^{-F^i}(1-Q_n)^{-1}\\
\cov(\phi_z(x)\cdot w_z^1,\phi_z(x)\cdot w_z^2) \quad \\
\quad \quad \le \tfrac{1}{2m}p^{F^{\cup}-(F^1+F^2)}(1-Q_n)^{-1}
\end{gather*}
\end{Lemma}

\begin{proof}
We'll prove the bias and variance results for $C=C^1$. The results for $C^2$ are
symmetric.
Given that $x=(x_1,\dots,x_d)$, let $C=x_{c_1}\vee\cdots\vee x_{c_F}$.

%Let $(r_z)_j$ be given by
%\begin{equation}
%(r_z)_j = \prod_{i=1}^F z_{c_i,j}^+-\prod_{i=1}^F z_{c_i,j}^-
%\end{equation}
We start by defining $Z_{j}^+ = \prod_{i=1}^F z_{c_i,j}^+$ and $Z_{j}^-= \prod_{i=1}^F z_{c_i,j}^-$
and then define $(r_z)_j = Z_j^+-Z_j^-$. The
following facts will be useful:
\vspace{-2pt}
%\begin{align*}
%\E[Z_j^+]&=\E[Z_j^-] = p^F\\
%\E[Z_j^+T_j^+] &= p^FC(x)+p^F(1-C(x))Q_n\\
%\E[Z_j^+T_j^-] &= p^FQ_n
%\end{align*}
\begin{align}
\label{eqn:useful1}\E[Z_j^+]&=\E[Z_j^-] = p^F\\
\label{eqn:useful2}\E[Z_j^+T_j^+] &= p^FC(x)+p^F(1-C(x))Q_n\\
\label{eqn:useful3}\E[Z_j^+T_j^-] &= p^FQ_n
\end{align}
\vspace{-2pt}
We have
\vspace{-1pt}
\begin{align*}
\E_{z}[\phi_z(x)\cdot r_z] &= %\sum_{j=1}^{m} \E_{z}[\phi_z(x)_j (r_z)_j]=
    m\E_{z}[\phi_z(x)_1(r_z)_1]\\
%\label{eqn:balance}
%&= m\E_z\left[\left(Z_1^+-Z_1^-\right)\left(T_1^+-T_1^-\right)\right]\\
&= 2m\E_z[T_1^+Z_1^+] - 2m\E_z[T_1^-Z_1^+]\\
%&= 2m\E_z[T_1^+Z_1^+] - 2m\E_z[T_1^-]\E_z[Z_1^+]\\
%&= 2m\E_z[T_1^+Z_1^+] - 2mp^FQ_n\\
&=2mp^F(1-Q_n)C(x)
\end{align*}
%where the equality \ref{eqn:balance} follows because the distribution of
%$\phi_z(x)_1z_{i,1}^-$ is the same as that of $\phi_z(x)_1z_{i,1}^+$. 
%The last line follows from equations \ref{eqn:useful1}-\ref{eqn:useful3}.

%The last
%equality follows from the fact that a case-by-case analysis: when $C(x)=1$, then
%$\phi_z(x)_1=1$ whenever $\prod_{i=1}^F z_{c_i,1}^+=1$. On the other hand,
%when $C(x)=0$ $\phi_z(x)_1$ is
%independent of $\prod_{i=1}^F z_{c_i,1}^+$ and so the two terms cancel.
For the variance, we compute
\begin{align*}
\var(\phi_z(x)\cdot r_z) &=% \sum_{i=1}^{m} \var(\phi_z(x)_j (r_z)_j) =
    m \var(\phi_z(x)_1 (r_z)_1)\\
\E[(\phi_z(x)_1 (r_z)_1)^2] &=
\E[(Z^+-Z^-)^2(T_1^+-T_1^-)^2]
%&= 2p^F(1-Q_n)C(x) + 4p^F(1-p^F)Q_n(1-Q_n)(1-C(x))
\end{align*}
from which we obtain (from equations \ref{eqn:useful1}, \ref{eqn:useful2}, \ref{eqn:useful3}):
\begin{align*}
\var(\phi_z(x)\cdot r_z) \le 4mp^F(1-Q_n)
\end{align*}
Now if we set $(w_z)_j = \frac{1}{2}p^{-F}(1-Q_n)^{-1}\frac{1}{m}(r_z)_j$ then
we recover the variance statement.

The covariance statement is computed similarly.
\end{proof}

%\begin{Lemma}\label{thm:covariance}
%Let $C(x)$ and $C'(x)$ be binary OR functions of fan-in $F$ and
%$F'$. Define $F^\cup$ as the number of inputs shared by $C$ and $C'$. Then
%the $w_z$ and $w_z'$ satisfying equations \ref{eqn:unbiased} and
%\ref{eqn:variance} for $C$ and $C'$ respectively also satisfy:
%\begin{gather}\label{eqn:covariance}
%\cov(\phi_z(x)\cdot w_z,\phi_z(x)\cdot w_z') \quad \\
%\quad \quad \le \tfrac{1}{2m}p^{F^{\cup}-(F+F')}(1-Q_n)^{-1}
%\end{gather}
%\end{Lemma}
%
%The proof of this Lemma \ref{thm:covariance} is very similar to that of the previous Lemma \ref{thm:biasvariance}.

\begin{Theorem}{\bf RKHS approximation}\label{thm:kernel}
Suppose $\phi_h$ is a Bloom feature map with $k$ hash functions. Suppose $K$ is
the kernel associated with the Reproducing Kernel Hilbert Space (RKHS)
$\mathcal{H}$ defined
below. Let $D_n\subset [0,1]^d$ be $D_n=\{x|\|x\|_0=n\}$.
%be the set of all vectors with $n$ non-zero
%entries. Then:
Then:
\begin{gather*}%\label{eqn:kernelexpectation}
\E_h[\langle \tfrac{1}{\sqrt{m}}\phi_h(x),\tfrac{1}{\sqrt{m}}\phi_h(y)\rangle] =
K(x,y)\\
%\end{equation}
%\begin{equation*}%\label{eqn:probability}
  \mathbb{P}\left(\sup_{x,y\in D_n} \left|\left\langle \tfrac{\phi_h(x)}{\sqrt{m}},\tfrac{\phi_h(y)}{\sqrt{m}}\right\rangle -
K(x,y)\right|>\epsilon\right)<\delta
\end{gather*}
as long as 
\[
  m>\tfrac{2}{\epsilon^2}\left(\log(1/\delta)+2\log\left(2\tbinom{d}{n}\right)\right)
\]

Where $K$ is given by the following construction:
For $x\in[0,1]^d$, let $\MAX_q(x)\in \R^{\binom{d}{q}}$ be the vector of
all $q$-ary MAX functions on the entries of $x$. Define
$\MAX_q=\R^{\binom{d}{q}}$. Finally, let $p=1-(1-\frac{1}{m})^k$ and define the RKHS $\mathcal{H}$ by
\begin{gather*}
\mathcal{H}\cong \bigoplus_{q=1}^d  \MAX_q\cong \R^{2^d}\\
\psi(x) = \bigoplus_{q=1}^d \sqrt{(1-p)^{d-q}p^q}\MAX_q(x)
\end{gather*}
For $v\in \mathcal{H}$ we write $v(x) = \langle
v,\psi(x)\rangle$ with kernel function $K(x,y)=\langle \psi(x),\psi(y)\rangle$
so that $\psi$ defines the norm on $\mathcal{H}$.

\end{Theorem}
%Notice that the definition of $\psi$ (and therefore $K$) heavily
%down-weights higher-order interactions among the variables. In particular,
%the function $f(x_1,\dots,x_n)=\MAX(x_1,\cdots,x_n)$ has norm $p^\frac{-d}{2}$ in $\mathcal{H}$. Further, it is
%not in fact feasible to actually compute $K$ for any reasonably large $d$
%or $n$. However computing $\phi_h$ is quite easy. The proof of Theorem
%\ref{thm:kernel} follows from
%an application of the Azuma-Hoeffding inequality.


\begin{proof}
First we compute the expectation:
\begin{align*}
\E_h[\langle\tfrac{1}{\sqrt{m}}\phi_h(x),\tfrac{1}{\sqrt{m}}\phi_h(y)\rangle]
&= \E_h[\phi_h(x)_1\phi_h(y)_1]
\end{align*}
Let $A_q$ be the event that there are exactly $q$ values of $t$ such that
$h_l(t)=1$ for some $l$. Then $P(A_q) = (1-p)^{d-q}p^q\binom{d}{q}$. Further,
\begin{align*}
\E_h[\phi_h(x)_1\phi_h(y)_1|A_q] &= 
  \tbinom{d}{q}^{-1} \langle\MAX_q(x),\MAX_q(y)\rangle
\end{align*}
%so that we end up with
\vspace{-10pt}
\begin{gather*}
    \E_h\left[\left\langle\tfrac{\phi_h(x)}{\sqrt{m}},\tfrac{\phi_h(y)}{\sqrt{m}}\right\rangle\right]=\\
    \sum_{q=1}^n P(A_q)\tbinom{d}{q}^{-1} \langle\MAX_q(x),\MAX_q(y)\rangle= K(x,y)
\end{gather*}

For the concentration inequality, we apply
the Azuma-Hoeffding inequality to the Doob martingale.
Let $B_i =
\frac{1}{m}\E[\langle\phi_h(x),\phi_h(y)\rangle|\phi_h(x)_1,\dots,\phi_h(y)_i]$.
Since each component of $\phi_h(x)$ is in $[0,1]$, $|B_{i+1}-B_{i}|\le 1$, and so
%we apply the Azuma-Hoeffding inequality
%to a Doob martingale: define $B_i =
%\frac{1}{m}\E[\langle\phi_h(x),\phi_h(y)\rangle|\phi_h(x)_1,\phi_h(y)_1,\dots,\phi_h(x)_i,\phi_h(y)_i]$. Then
%$B_i$ is a Doob martingale with $B_0 = K(x,y)$ and $B_m=\frac{1}{m}\langle
%\phi_h(x),\phi_h(y)\rangle$. Also, clearly $|B_i-B_{i-1}|\le \frac{1}{m}$ since
%$0\le \frac{1}{m}\phi_h(x)_i\phi_h(y)_i\le \frac{1}{m}$. Therefore
\begin{align*}
    P\left(\left|\left\langle\tfrac{\phi_h(x)}{\sqrt{m}},\tfrac{\phi_h(y)}{\sqrt{m}}\right\rangle-K(x,y)\right|>\epsilon\right)\le
    2\exp(-\tfrac{\epsilon^2m}{2})
%P(|-B_0|\ge \epsilon)\le 2\exp(-\epsilon^2m/2)
\end{align*}
The maximum error occurs at corners of $D_n$, so by applying a union bound
over all $\binom{d}{n}^2$ pairs $x,y$ we bound the probability of deviation by
$$
2\tbinom{d}{n}^2\exp(-\epsilon^2m/2)=\delta
$$
Solve for $m$ to prove the theorem.
%, we have
%\begin{align*}
%P_h(\sup_{x,y\in D_n} |\langle
%\frac{1}{\sqrt{m}}\phi_h(x),\frac{1}{\sqrt{m}}\phi_h(y)\rangle -
%K(x,y)|\ge \epsilon)&\le 2{d\binom n}\exp(-\epsilon^2m/2)
%\end{align*}
%If we set this value equal to $\delta$ and solve for $m$ we obtain
%\[
%m \ge \frac{2}{\epsilon^2}\left(\log(1/\delta)+\log(2{d\binom n})\right)
%\]
\end{proof}


