ieee:
	pdflatex bloom_ieee_main.tex
	bibtex bloom_ieee_main
	pdflatex bloom_ieee_main.tex
	pdflatex bloom_ieee_main.tex


publish:
	pdflatex bloomfeatures.tex
	bibtex bloomfeatures
	pdflatex bloomfeatures.tex
	pdflatex bloomfeatures.tex
	../publish.sh .

clean:
	rm -f *.aux *.log *.out *.pdf
